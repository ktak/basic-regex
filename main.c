#include <stdio.h>

int Regex(const char *regex, const char *str);

int main(int argc, char **argv)
{
    int test = 1;
    if(!Regex("", ""))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(Regex("", "a"))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(Regex("a", ""))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(!Regex("a*", ""))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(!Regex("a*", "a"))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(!Regex("a*", "aaa"))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(Regex("a*", "b"))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(!Regex("a*b*", ""))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(!Regex("a*b*", "a"))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(!Regex("a*b*", "aabb"))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(!Regex("a*b*", "bb"))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(Regex("a*b*", "ba"))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(!Regex("a|b", "a"))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(!Regex("a|b", "b"))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(Regex("a|b", ""))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(!Regex("a|b*", "bb"))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(!Regex("a*|b*", "aaa"))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(!Regex("a|b*|c*", "cc"))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(!Regex("a|b*|c*", ""))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(!Regex("a|b*c*d*e", "bbccccccccccccccccccccccccccccccccccccccccccccccccccdddddddddddddddddddddddddddddddddddddddddddddddde"))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(Regex("(a)", ""))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(!Regex("(a)", "a"))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(!Regex("(a)*", ""))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(!Regex("(a)*", "a"))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(!Regex("(a)*", "aaa"))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(!Regex("(a|b)*", ""))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(!Regex("(a|b)*", "abba"))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(!Regex("(a|b|c|d)*", "abcdabcdabcd"))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    if(!Regex("(0|1|2|3|4|5|6|7|8|9)*", "0123456789"))
    {
        fprintf(stderr, "Test %d failed\n", test);
    }
    test++;
    return 0;
}
